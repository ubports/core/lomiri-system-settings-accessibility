# Uyghur translation for lomiri-system-settings-accessibility
# Copyright (c) 2013 Rosetta Contributors and Canonical Ltd 2013
# This file is distributed under the same license as the lomiri-system-settings-accessibility package.
# Gheyret Kenji <gheyret@gmail.com>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: lomiri-system-settings-accessibility\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-12-18 11:12-0600\n"
"PO-Revision-Date: 2014-10-19 01:58+0000\n"
"Last-Translator: Gheyret T.Kenji <Unknown>\n"
"Language-Team: Uyghur Computer Science Association <UKIJ@yahoogroups.com>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Launchpad-Export-Date: 2015-07-16 05:42+0000\n"
"X-Generator: Launchpad (build 17628)\n"

#. TRANSLATORS: This is a keyword or name for the accessibility plugin which is used while searching
#: ../build/po/settings.js:38
msgid "Accessibility"
msgstr "ياردەم ئىقتىدارى"

#. TRANSLATORS: This is a keyword or name for the accessibility plugin which is used while searching
#: ../build/po/settings.js:40
msgid "accessibility"
msgstr "قوشۇمچە ئىقتىدارى"

#. TRANSLATORS: This is a keyword or name for the accessibility plugin which is used while searching
#: ../build/po/settings.js:42
msgid "a11y"
msgstr "a11y"

